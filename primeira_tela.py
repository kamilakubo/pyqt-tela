import sys

from PyQt5.QtWidgets import (
    QMainWindow,
    QApplication,
    QVBoxLayout,
    QLabel,
    QLineEdit,
    QWidget,
    QSpacerItem,
    QSizePolicy,
    QPushButton
)

class FirstForm(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.lbl_name = QLabel()
        self.lbl_name.setText('Nome')



        self.edt_name = QLineEdit()

        self.btn_add = QPushButton()
        self.btn_add.setText('Incluir')
        self.btn_add.clicked.connect(self._on_add)


        space = QSpacerItem(0, 0, QSizePolicy.Fixed,QSizePolicy.Expanding)

        self.layout = QVBoxLayout()
        self.layout.setSpacing(12)
        self.layout.addWidget(self.lbl_name)
        self.layout.addWidget(self.edt_name)
        self.layout.addWidget(self.btn_add)


        self.layout.addItem(space)

        self.panel = QWidget()
        self.panel.setLayout(self.layout)

        self.setCentralWidget(self.panel)

        self.setWindowTitle('Primeira Tela')
        self.setGeometry(20, 200, 800, 400)

    def _on_add(self):

        print(f'Adicionado {self.edt_name.text()}')


app= QApplication(sys.argv)
main = FirstForm()
main.show()
sys.exit(app.exec_())
